# README #

简单密码

### What is this repository for? ###

* 这是一个chrome插件
* Version 1.0.0.0
* [Learn Markdown](https://bitbucket.org/billy119/easypassword)

### How do I get set up? ###

* 为了防止密码泄露，根据自己设定的密钥和当前网页的域名(如www.zhihu.com，则会以zhihu为域名)作为密钥进行双重加密，生成固定长度的密码。
* 此密码无需记忆，只要有自己固定的密钥，就可以随时通过此插件获取密码。
* http://findicons.com/
* Dependencies
* 无数据库操作，保证使用者的密码不会在此泄露
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact